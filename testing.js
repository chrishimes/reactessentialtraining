var rsq1 = ASQ.react.of();
var rsq2 = ASQ.react.of(1,2,3);
var x = 10;

setInterval(function(){
  rsq1.push(x++);
  rsq2.push(x++);
}, 500);

rsq1.val(function(v){
  console.log("1:", v);
});

rsq2.val(function(v){
  console.log("2", v);
});
